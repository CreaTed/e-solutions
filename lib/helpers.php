<?php
    $errores = array();
    $registrado = false;

    function getRegistrado()
    {
        global $registrado;

        return $registrado;
    }
    function setRegistrado($estate)
    {
        global $registrado;

        $registrado = $estate; 
    }
    function getUrl($modulo, $controlador, $funcion, $parametros=false) 
    {
        $url = "index.php?modulo=$modulo&controlador=$controlador&funcion=$funcion";
    
        if($parametros!=false)
        {
            foreach($parametros as $key=>$valor)
            {
                $url .="&$key=$valor";
            }
        }
    
        return $url;
    }
    function dd($variable)
    {
        echo "<pre>";
        die(print_r($variable));
    }

    function redirect($url)
    { 
        echo "<script type='text/javascript'>window.location.href='$url'</script>";
    }
    function console($object)
    {
        $console = "<script type='text/javascript'>console.log('". $object . "');</script>";

        echo $console;
    }
    function addError($str)
    {
        global $errores;

        $errores[] = $str;
    }
    function getErrorCount()
    {
        global $errores;
        return count($errores);
    }
    function getTheErrors()
    {
        global $errores;

        return $errores;    
    }
    function resultBlock()
	{
        global $errores;
		if ( getErrorCount() > 0 )
		{
			echo "<div id='error' class='alert alert-danger' role='alert'>
				<a href='#' onclick=\"showHide('error');\">[x]</a>
				<ul>";
				foreach($errores as $error)
				{
					echo "<li>" .$error. "</li>";
				}
				echo "</ul>";
				echo "</div>";
        }
        else
        {
            setRegistrado(true);
        }
			
    }
    function clearErrors()
    {
        global $errores;
        $errores = array();
    }
?>