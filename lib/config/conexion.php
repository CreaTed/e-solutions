<?php

class Conexion {
    
    private $host;
    private $user;
    private $password;
    private $db;
    private $port = 5432;
    private $con = null;

    function __construct() 
    {
        $this->host = "localhost";
        $this->user = "root";
        $this->password = "";
        $this->db = "e-solutions";

        $this->con = mysqli_connect($this->host, $this->user, $this->password, $this->db);

        if(!$this->con)
            die("Ocurrio un error al intentar la conexion");
    }
    public function cerrar()
    {
        mysqli_close($this->con);
    }
    public function getConexion()
    {
        return $this->con;
    }
    
}
