<?php

include_once "../model/Dashboard/dashboardModel.php";

class dashboardController
{
    private $patron_texto = "/^[a-zA-Z0-9\sáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙ]+$/";

    function getPos($array, $value)
    {
        for($i = 0; $i < count($array); $i++)
        {
            if($array[$i]['CODIGO'] == $value)
            {
                return $i;
            }
        }

        return 0;
    }
    function pickRandomColor()
    {
        $colors = array('#F06292','#64FFDA','#40C4FF','#40C4FF');

        return $colors[rand(0,3)];
    }
    function getBgColor( $bgcolor )
    {
        $arrColor = array( "NEGRO"  => "black",
                           "BLANCO" => "white",
                           "AZUL"   => "blue"
                         );
        
        foreach( $arrColor as $color => $clr )
        {
            if( $color == $bgcolor )
            {
                $hexCol = $clr;
                break;
            }
        }
    
        return $hexCol;
    }
    function truncString($str, $max)
    {
       $add = "...";
     
       if (strlen($str) > $max)
       {
           $truncText = substr($str, 0, $max) . $add;
          
           return $truncText;
       }
       else
       {
          return $str;  
       }
    }
    function listarCarro()
    {
        $objDash = new dashboardModel();
        $selectCompras =   " SELECT PD.NOMBRE, PD.PRECIO, IMG.URL "
                          .", ( SELECT SUM(PT.PRECIO)             "
                          ."  FROM PRODUCTO PT                    "
                          ."  INNER JOIN carro_compras CP         "
                          ."  ON PT.CODIGO = CP.CODPROD           "
                          ."  WHERE CP.CODUSU = CC.CODUSU) TOTAL  "  
                          .", CC.CODIGO                           "          
                          ." FROM CARRO_COMPRAS CC                "        
                          ." INNER JOIN PRODUCTO PD               "       
                          ." ON PD.CODIGO = CC.CODPROD            "     
                          ." INNER JOIN PRODUCTO_IMAGEN IMG       "   
                          ." ON PD.CODIGO = IMG.CODPROD           "   
                          ." WHERE CC.CODUSU = ?                  ";
        $params = array();
        array_push($params, '1' );
        $productos = $objDash->select($selectCompras, True, 1, 's', $params);
        $numrows = $objDash->getNumRows("CARRO_COMPRAS");
        $objDash->cerrar(); 

        include_once "../view/Dashboard/carro.php";
    }
    function pago()
    {
        $objDash = new dashboardModel();
        $delete = " DELETE FROM CARRO_COMPRAS "
                 ." WHERE CODUSU = ?          ";
        $params = array();
        array_push($params, '1' );
        $objDash->delete($delete, True, 1, 's', $params);
        $objDash->cerrar();   
        $this->listar();          
    }
    function eliminarProdCarro()
    {
        $id = $_GET['id'];
        $objDash = new dashboardModel();
        $delete = " DELETE FROM CARRO_COMPRAS "
                 ." WHERE CODIGO = ?          ";
        $params = array();
        array_push($params, $id );
        $objDash->delete($delete, True, 1, 's', $params);
        $objDash->cerrar();   
        $this->listarCarro();     

    }
    function carrito()
    {
        $objDash = new dashboardModel();
        $codProd = $_GET['id'];
		$stmt = mysqli_prepare( $objDash->getConexion(), "SELECT CODPROD FROM CARRO_COMPRAS WHERE CODPROD = ? ");
		mysqli_stmt_bind_param($stmt, "s", $codProd);
		mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $existe);
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_free_result($stmt);
        mysqli_stmt_close($stmt); 
            
        if($existe == "")
        {
            $objDash = new dashboardModel();
            $getSeqCarro = $objDash->getSeq("CARRO_COMPRAS");
            $insertCarro = " INSERT INTO CARRO_COMPRAS VALUES( ?, ?, ?) ";                
            $params = array();
            array_push($params, $getSeqCarro,  '1', $codProd );
            $objDash->insertar($insertCarro, True, 3, 'sss', $params);
        }
        $objDash->cerrar();
        $this->listar();
    }
    function listar()
    {
        $selectSql = " SELECT PD.CODIGO, PD.NOMBRE, PD.PRECIO, PD.DESCRIPCION, IMG.URL "
        ."FROM PRODUCTO PD "
        ."INNER JOIN PRODUCTO_IMAGEN IMG ON PD.CODIGO = IMG.CODIGO ";
    
        $objDash = new dashboardModel();
        $productos   = $objDash->select($selectSql);
        $numrows = $objDash->getNumRows("PRODUCTO");
        $dcto = false;
        $objDash->cerrar(); 

        include_once "../view/Partials/dashboard.php";
    }
    function detalle()
    {
        $objDash = new dashboardModel();

        $id = $objDash->real_escape_string($_GET['id']);

        $selectProd = " SELECT PD.CODIGO, PD.NOMBRE, PD.PRECIO, PD.DESCRIPCION " 
                         .", IMG.URL FROM PRODUCTO PD "
                         ."INNER JOIN PRODUCTO_IMAGEN IMG ON PD.CODIGO = IMG.CODIGO "
                         ." WHERE PD.CODIGO = ? ";

        $SelectnxtPrev = "SELECT CODIGO FROM PRODUCTO";

        $params = array();
        array_push($params, $id);
        //productos
        $producto   = $objDash->find($selectProd, True, 1, 's', $params);        
        $nxtPrev = $objDash->select($SelectnxtPrev); 

        //traer el codigo siguiente y el anterior de sql para hacer slider con el detalle
        //de los productos
        $max = count($nxtPrev);
        $next     = $nxtPrev[$this->getPos($nxtPrev, $id)]['CODIGO'];
        $previous = $nxtPrev[$this->getPos($nxtPrev, $id)]['CODIGO'];

        if($next == $nxtPrev[$max-1]['CODIGO'])
            $next = $nxtPrev[0]['CODIGO'];
        else
            $next = $nxtPrev[$this->getPos($nxtPrev, $id) + 1]['CODIGO'];

        if($previous == $nxtPrev[0]['CODIGO'])
            $previous = $nxtPrev[$max-1]['CODIGO'];
        else
            $previous= $nxtPrev[$this->getPos($nxtPrev, $id) - 1]['CODIGO']; 

        $index = 1;
        $objDash->cerrar();
        $params = array();
        include_once "../view/Dashboard/detalle.php";
    }
    
}
