<?php

    include_once "../model/Admin/adminModel.php";

    class adminController
    {            
        private $patron_texto = "/^[a-zA-Z0-9\sáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙ._-]+$/";

        function listar()
        {
            $selecProductos = " SELECT PD.CODIGO, PD.NOMBRE, PD.PRECIO, PD.DESCRIPCION, IMG.URL "
            ."FROM PRODUCTO PD "
            ."INNER JOIN PRODUCTO_IMAGEN IMG ON PD.CODIGO = IMG.CODIGO ";

            $objAdmin = new adminModel();

            $productos  = $objAdmin->select($selecProductos);
            $objAdmin->cerrar(); 
            include_once '../view/Admin/listarProductos.php';
        } 
        function crearProductos()
        {
            include_once '../view/Admin/crearProductos.php';    
        } 
        function crearProductoPost()
        {

            $objAdmin = new adminModel();

            //Faja
            if( isset( $_POST['nombre'] ) && Trim($_POST['nombre']) != "" )
                $prodNom = $objAdmin->real_escape_string($_POST['nombre']);
            else
                addError("Debe ingresar el nombre del producto!");
            
            if( !preg_match($this->patron_texto, $prodNom) )
                addError(" El nombre del producto sólo pueden contener letras y espacios! ");
            
            if( isset( $_POST['precio'] ) && Trim($_POST['precio']) != "" )
                $prodPre = $objAdmin->real_escape_string($_POST['precio']);
            else
                addError("Debe ingresar el precio del producto!");
            
            if(!is_numeric($prodPre))
                addError(" El precio debe ser un valor numerico! ");

            if( isset( $_POST['descripcion'] ) && Trim($_POST['descripcion']) != "" )
                $prodDes = $objAdmin->real_escape_string($_POST['descripcion']);
            else
                addError("Debe agregar la descripcion del producto!");
            
            if( !preg_match($this->patron_texto, $prodDes) )
                addError(" La descripcion del producto sólo pueden contener letras y espacios! ");
            
            //imagen
            if( isset( $_POST['uploadedImage'] ) && Trim($_POST['uploadedImage']) != "" )
                $filename = $objAdmin->real_escape_string($_POST['uploadedImage'] ); 
            else
                addError("Debe cargar la imagen del producto");
            
            $ext = pathinfo($filename, PATHINFO_EXTENSION);

            if( strcmp($ext, "jpg")  != 0 && strcmp($ext, "jpeg") != 0  && strcmp($ext, "png") != 0  )
                addError("La aplicacion solo soporta los tipos jpg, jpeg, png ");
                    

            if( getErrorCount() > 0 )
            {
                $this->crearProductos();                
            }
            else
            {               

                if( getErrorCount() > 0 )
                {
                    $this->crearProductos();                
                }
                else
                {
                    $objAdmin->beginTransaction();
                    $getSeqProd = $objAdmin->getSeq("PRODUCTO");                
                    $params = array();
                    array_push($params, $getSeqProd,  $prodNom, $prodPre, $prodDes );
                    $InsertProd = " INSERT INTO PRODUCTO VALUES( ?, ?, ?, ?)";
                    $faja = $objAdmin->insertar($InsertProd, True, 4, 'ssds', $params);

                    //insertar imagen
                    $getSeqImg = $objAdmin->getSeq("PRODUCTO_IMAGEN");                
                    $params = array();
                    array_push($params, $getSeqImg,  "img/". $filename, $getSeqProd);
                    $InsertImagen = " INSERT INTO PRODUCTO_IMAGEN VALUES( ?, ?, ? ) "; 
                    $imagen = $objAdmin->insertar($InsertImagen, True, 3, 'sss', $params); 
                        
                    if( $faja === false || $imagen === false )
                        $faja = $objAdmin->rollBack();
                    else
                        $faja = $objAdmin->Commit();

                    $params = array();
                    $objAdmin->cerrar();

                    redirect(getUrl("admin", "admin", "listar"));
                }
                
            }
             
        }
        function editar( $hasError = False, $id_Error = 0 )
        {
            $objAdmin = new adminModel();
            
            if($hasError)
                $id = $id_Error;
            else
                $id = $_GET['id'];

            $SelectProd= " SELECT PD.CODIGO, PD.NOMBRE, PD.PRECIO, PD.DESCRIPCION, IMG.URL "
            ."FROM PRODUCTO PD "
            ."INNER JOIN PRODUCTO_IMAGEN IMG ON PD.CODIGO = IMG.CODIGO "
            ."AND PD.CODIGO = ?";
            
            $params = array();
            array_push($params, $id);                        

            $producto = $objAdmin->find( $SelectProd, True, 1, 's', $params );            
            $objAdmin->cerrar();
            $params = array();
            include_once '../view/Admin/editarProductos.php';        
        }
        function editarProductoPost()
        {
            $objAdmin = new adminModel();

            $id = $_POST['id'];

            //Faja
            if( isset($_POST['nombre']) && Trim($_POST['nombre']) != "" )
                $prodNom = $objAdmin->real_escape_string($_POST['nombre']);
            else
               addError("El producto debe tener un nombre!");
            
            if( !preg_match($this->patron_texto, $prodNom) )
                addError(" El nombre del producto sólo pueden contener letras y espacios! ");

            if( isset($_POST['precio']) && Trim($_POST['precio']) != "" )    
                $prodPre = $objAdmin->real_escape_string($_POST['precio']);
            else
                addError("El producto debe tener un precio");
            
            if(!is_numeric($prodPre))
                addError(" El precio debe ser un valor numerico! ");

            if( isset($_POST['descripcion']) && Trim($_POST['descripcion']) != "" )     
                $prodDes = $objAdmin->real_escape_string($_POST['descripcion']);
            else 
               addError("El producto debe tener una descripcion");
            
            if( !preg_match($this->patron_texto, $prodDes) )
                addError(" La descripcion del producto sólo pueden contener letras y espacios! ");
            
            if(getErrorCount() > 0)
            {
                $this->editar(True, $id);
            }
            else
            {
                $sqlUpdate = " UPDATE PRODUCTO SET NOMBRE = ? "
                            ." ,PRECIO = ? "
                            ." ,DESCRIPCION = ? "
                            ." WHERE CODIGO = ? ";
                $params = array();
                array_push($params, $prodNom, $prodPre, $prodDes, $id);
                $update = $objAdmin->update( $sqlUpdate, True, 4, 'sdss', $params );
            
                $params = array();                       
                $objAdmin->cerrar();
                redirect(getUrl("admin", "admin", "listar"));
            }            
        }
        function deshabilitar()
        {   
            $objAdmin = new adminModel();

            $id = $_GET['id'];
            $updateEstado = " UPDATE FAJAS SET ESTADO = 1 WHERE CODIGO = $id";
            $fajas   = $objAdmin->update($updateEstado);
            $objAdmin->cerrar(); 

            redirect(getUrl("admin", "admin", "listar"));
        }  
        function habilitar()
        {
            $objAdmin = new adminModel();
            $id = $_GET['id'];
            $updateEstado =" UPDATE FAJAS SET ESTADO = 0 WHERE CODIGO = $id";
            $fajas   = $objAdmin->update($updateEstado);
            $objAdmin->cerrar(); 

            redirect(getUrl("admin", "admin", "listar"));
        }
    }

?>