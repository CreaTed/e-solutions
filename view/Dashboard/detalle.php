
<div class="container" id="detalle">
    <button class="display-left" onclick="RestDivs(<?php echo $previous ?>)">&#10094;</button>
    <div class="imgPreview">
        <img src="<?php echo $producto['URL'] ?>"/>
    </div>
    <button class="display-right" onclick="plusDivs(<?php echo $next ?>)">&#10095;</button>
        <div class="informacion">
            <h1 style="text-align: center; background-color:<?php echo $this->pickRandomColor() ?>"><b><?php echo StrToUpper($producto['NOMBRE']) ?></b></h1>
            <P>$ <?php echo number_format($producto['PRECIO']) ?></p>
            <p><b>DESCRIPCION:</b> <br><?php echo StrToUpper($producto['DESCRIPCION']) ?></p>
                <div class="detalle-footer">
                    <p class="info">¿Te gustaria un color en especial? Contactate con nosotros y pidelo!</p>
                    <h3>Contactenos en</h3>
                    <div class="footer_social">
                        <ul>
                            <li><a class="f_facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a class="f_twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a class="f_google" href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a class="f_linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
        </div>        
</div>  


