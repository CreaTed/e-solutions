<div class="filtros">
<?php
foreach($fajas as $faja)
    { 
    ?>
        <a href="<?php echo getUrl("dashboard", "dashboard", "detalle", array("id"=>$faja['CODIGO']))  ?>">
            <div class="container" id="faja" style="border-bottom: 2px solid <?php echo $this->pickRandomColor() ?>">
                <div class="precio">
                        <p><?php echo StrToUpper(number_format($faja['PRECIO'])) ?> $ </p>
                </div>
                <div class="fajaTitulo">
                    <h3 style="background-color:<?php echo $this->pickRandomColor() ?>"><?php echo StrToUpper($this->truncString($faja['FAJANOM'], 10)) ?></h3>
                </div>
                <div class="fajaImg">
                    <div class="fajaUrl">
                        <img src="<?php echo $faja['URL'] ?>"/>
                    </div>
                </div>
                <div class="faja-footer">
                    <p class="descripcion"><?php echo StrToUpper($faja['DESCRIPCION']) ?></p>
                    <!--<a href="<?php echo getUrl("dashboard", "dashboard", "detalle", array("id"=>$faja['CODIGO']))  ?>" class="btn btn-primary"> DETALLE </a>-->
                </div>
            </div>
        </a>
<?php
    }
?>
</div>