<!-- CONTENT START -->
<div id="carro-section" class="section-padding">
    <h2 style="color: #b02f30; text-align: center;">Lista de productos agregada al carro</h2>
        <div class="container" id="admin">
            <div id="lista">
                <?php if( $numrows > 0 )
                {
                ?>
                    <div class="table-responsive">          
                        <table class="table table-striped">
                            <thead style="background-color: #1e1e1e">
                                <tr>
                                    <th style="color: white;">Imagen</th>
                                    <th style="color: white;">Nombre</th>
                                    <th style="color: white;">Precio</th>
                                    <th style="color: white;">Remover</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($productos as $producto)
                                {   
                            ?>
                                    <td><img src="<?php echo $producto['URL'] ?>"/></td>
                                    <td><?php echo $producto['NOMBRE'] ?></td>
                                    <td><?php echo $producto['PRECIO'] ?></td>
                                    <td><a href="<?php echo getUrl("dashboard", "dashboard", "eliminarProdCarro", array("id"=>$producto['CODIGO'])) ?>"><span class="glyphicon glyphicon-remove" title="remover"></span></a></td>
                                </tr>
                            <?php
                                }
                            ?>
                            <td></td>
                            <td></td>
                            <td><strong>Total: </strong><?php echo $producto['TOTAL'] ?></td>
                            <td><a class="btn btn-primary" href="#modal" onclick="ShowPay()">Pagar</a></td>
                            </tbody>
                        </table>
                    </div>
                <?php 
                }else
                    echo "no hay productos agregados al carro"
                ?>
            </div>
        </div>