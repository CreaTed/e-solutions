<div class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="max-width: 400px">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">AGUITA DE CODIGO</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="modal-logo">
                    <img src="../web/img/logo.png"/>  
                </div>

                <hr>
                <ul class="nav nav-tabs" id="tabContent">
                    <li class="active"><a href="#login" data-toggle="tab" style="padding: 3px; text-decoration: none;">Pagos</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="login">
                        <form name="carroCompras" action="index.php?modulo=dashboard&controlador=dashboard&funcion=pago" onsubmit="return validarPago()" method="post">
                            <div class="form-group">
                                <label for="email">Credit car: </label>
                                <input type="text" class="form-control" id="creditcard" name="creditcard" placeholder="Ingrese su tarjeta de credito" required>
                            </div>

                            <div class="form-group">
                                <label for="passord">Vigencia: </label>
                                <input type="text" class="form-control" id="vigencia" name="vigencia" placeholder="Ingrese la vigencia mm/yy" required>
                            </div>

                            <div class="form-group">
                                <label for="passord">Numeros de seguridad: </label>
                                <input type="text" class="form-control" id="nseguridad" name="nseguridad" placeholder="Ingrese los numeros de seguridad" required>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" >Pagar</a>                            
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>