<div class="container" id="mainc">
    <div class="container" id="productos">
    <?php
        foreach($productos as $producto)
        { 
    ?>
        <a href="<?php echo getUrl("dashboard", "dashboard", "detalle", array("id"=>$producto['CODIGO']))  ?>">
            <div class="product-container">
                <div class="border-cn">
                    <div class="product-image-cn">
                        <img src="<?PHP echo $producto['URL']?>"/>    
                    </div>
                    <?php 
                        $fecha_actual = strtotime(date("d/m/y"));
                        $fecha_ini    = strtotime("10/12/2018");
                        $fecha_fin    = strtotime("12/12/2018");
                        if( $fecha_actual >= $fecha_ini && $fecha_actual <= $fecha_fin )
                        {
                            $dcto = true;
                    ?>
                        <div class="product-descuento">
                            <h5>15% DCTO</h5>
                        </div>
                    <?php 
                        }
                    ?>
                    <h5 style="margin-left:10px; text-align:left;"> <strong><?php echo $producto['NOMBRE']?></strong></h5>
                    <div class="product-precio">
                        <h4><strong>$ <?PHP echo $producto['PRECIO'] ?></strong></h4>
                        <?php 
                            if($dcto)
                            {
                                $porc = $producto['PRECIO'] * 15 /100;
                                $precio = $producto['PRECIO'] - $porc;
                        ?>
                            <h5 style="color: red"> con dcto <?php echo $precio ?> </h5>
                        <?php
                            }
                        ?>
                    </div>
                    <div class="product-details">
                        <p><?php echo $producto['DESCRIPCION'] ?></p>
                    </div>
                    <div class="product-button ">
                        <a class="btn btn-danger" href="<?php echo getUrl("dashboard", "dashboard", "carrito", array("id"=>$producto['CODIGO']))  ?>">Agregar al carrito</a>
                    </div>
                </div>
            </div>     
        </a> 
    <?php
        } 
    ?>
    </div>
</div>