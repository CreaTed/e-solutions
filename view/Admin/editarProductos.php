<div id="creaFaja" class="section-padding">
<?php
if ( getErrorCount() > 0  )
{
    $errores = array();
    $errores = getTheErrors();
?>
<div id="error-alert" class="alert alert-danger alert-dismissible fade in">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Errores: </strong> 
<?php 
    for($i = 0; $i < getErrorCount(); $i++)
    {
        echo "<li>" .$errores[$i]. "</li>";
    }
    clearErrors();
    $errores = array();    
?>
</div>
<?php 
}
?>
    <div class="container" id="editarProducto"> 
        <div class="imgFaja">
            <img src="<?php echo $producto['URL'] ?>" name="uploadedImage"/>
        </div>
        <div class="faja">
            <form action="<?php echo getUrl("admin", "admin", "editarProductoPost") ?>" method ="POST">
            <input type="hidden" name="id" value ="<?php echo $id ?>"/>
            <div class="fajad">
                <div class="form-group">
                    <label for="FAJANOM">Nombre de la faja</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $producto['NOMBRE'] ?>">
                </div>
                <div class="form-group">
                    <label for="PRECIO">Precio de la faja</label>
                    <input type="text" class="form-control" id="precio" name="precio" value="<?php echo $producto['PRECIO'] ?>">
                </div>
                <div class="form-group">
                    <label for="DESCRIPCION">Descripcion de la faja</label>
                    <textarea class="form-control" rows="5" id="descripcion" name="descripcion"><?php echo $producto['DESCRIPCION'] ?></textarea>
                </div>
            </div>
            <div class="tajabutton">
                <div class="form-group">
                    <input class="btn btn-primary" type="submit" value="Modificar" name="submit" style="background: #e43c5c; width: 100px;">
                </div>
            </div>
        </form>    
        </div>
    </div>