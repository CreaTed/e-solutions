<div id="creaFaja" class="section-padding">
<?php
if ( getErrorCount() > 0  )
{
    $errores = array();
    $errores = getTheErrors();
?>
<div id="error-alert" class="alert alert-danger alert-dismissible fade in">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Errores: </strong> 
<?php 
    for($i = 0; $i < getErrorCount(); $i++)
    {
        echo "<li>" .$errores[$i]. "</li>";
    }
    clearErrors();
    $errores = array();    
?>
</div>
<?php 
}
?>
    <div class="container" id="crearFaja">
        <div class="faja">
            <form action="<?php echo getUrl("admin", "admin", "crearProductoPost") ?>" method ="POST">
                <div class="imgFaja">
                    <div class="form-group">
                        <p>Seleccionar imagen a subir:</p>
                        <input type="file" id="image" name="uploadedImage" accept=".jpg, .jpeg, .png"/>
                        <img id="img-preview" src="#" name="uploadedImage" />
                    </div>
                </div>
                <div class="fajad">
                    <div class="form-group">
                        <label for="FAJANOM">Nombre del producto</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingrese el nombre del producto">
                    </div>
                    <div class="form-group">
                        <label for="PRECIO">Precio del producto</label>
                        <input type="text" class="form-control" id="precio" name="precio" placeholder="Ingrese el precio">
                    </div>
                    <div class="form-group">
                        <label for="DESCRIPCION">Descripcion del producto</label>
                        <textarea class="form-control" rows="5" id="descripcion" name="descripcion"></textarea>
                    </div>
                </div>
                <div class="tajabutton">
                    <div class="form-group">
                        <input class="btn btn-primary" type="submit" value="Crear" name="submit" style="background: #e43c5c; width: 100px;">
                    </div>
                </div>
            </form>    
        </div>  
    </div>
</div>