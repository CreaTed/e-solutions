<!-- CONTENT START -->
    <div id="admin-section" class="section-padding">
        <div class="container" id="admin">
            <div id="sidebar" class="sidenav" style="width: 0px;">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                <h1>MODULOS</h1>
                <a href="#">Productos</a>
            </div>
            <span id="opnMenu" class="glyphicon glyphicon-th-list" onclick="openNav()"></span>
            <div id="lista">
                <div class="form-group">
                    <a href="<?php echo getUrl("admin", "admin", "crearProductos") ?>" class="btn btn-success">NUEVO PRODUCTO</a>
                </div>
                <div class="table-responsive">          
                    <table class="table table-striped">
                        <thead style="background-color: #1e1e1e">
                            <tr>
                                <th style="color: white;">Imagen</th>
                                <th style="color: white;">Nombre</th>
                                <th style="color: white;">Precio</th>
                                <th style="color: white;">Descripcion</th>
                                <th colspan=2 style="color: white;">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($productos as $producto)
                            {   
                        ?>
                                <td><img src="<?php echo $producto['URL'] ?>"/></td>
                                <td><?php echo $producto['NOMBRE'] ?></td>
                                <td><?php echo $producto['PRECIO'] ?></td>
                                <td><?php echo $producto['DESCRIPCION'] ?></td>
                                <td><a href="<?php echo getUrl("admin", "admin", "editar", array("id"=>$producto['CODIGO'])) ?>"><span class="glyphicon glyphicon-pencil" title="editar"></span></a></td>
                            </tr>
                        <?php
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>