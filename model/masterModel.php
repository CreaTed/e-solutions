<?php

include_once('../lib/config/conexion.php');

class MasterModel extends Conexion{
    
    function bindParams( $varTypes, $nParams, $params = array(), $stmt )
    {
        $a_Params = array();
        $a_Params[] = & $varTypes;

        for( $i = 0; $i < $nParams; $i++ )
            $a_Params[] = & $params[$i];
        
        call_user_func_array(array($stmt, 'bind_param'), $a_Params);
    }

    function insertar( string $sql, bool $hasParams = False, int $nParams = 0, string $varTypes = '', array $params = array() )
    {
        $stmt = mysqli_prepare( $this->getConexion(), $sql );

        if($hasParams)
        {
            $this->bindParams( $varTypes, $nParams, $params, $stmt );
        }

        $insert = mysqli_stmt_execute($stmt);
        mysqli_stmt_free_result($stmt);

        return $insert;
    }
    
    function select( string $sql, bool $hasParams = False, int $nParams = 0, string $varTypes = '', array $params = array() )
    {
		$stmt = mysqli_prepare( $this->getConexion(), $sql );

        if($hasParams)
        {
            $this->bindParams( $varTypes, $nParams, $params, $stmt );
        }

        mysqli_stmt_execute($stmt);

        $respuesta = mysqli_stmt_get_result($stmt);
         
        $return = array();
        
        if ($respuesta) {
            while ($row = @mysqli_fetch_array($respuesta)) {
                if(isset($row['estado']) ){
                    if($row['estado']==null){
                        $return[]=$row;
                    }
                }else{
                    $return[]=$row;
                }
            }
        }
        mysqli_stmt_free_result($stmt);
        
        return $return;
    }
    
    function find( string $sql, bool $hasParams = False, int $nParams = 0, string $varTypes = '', array $params = array() )
    {
        $stmt = mysqli_prepare( $this->getConexion(), $sql );

        if($hasParams)
        {
            $this->bindParams( $varTypes, $nParams, $params, $stmt );
        }

        mysqli_stmt_execute($stmt);

        $respuesta = mysqli_stmt_get_result($stmt);
        
        $return = false;
        if ($respuesta) {
            $return = array();
            while ($row = @mysqli_fetch_array($respuesta)) {
                if(isset($row['estado']) ){
                    if($row['estado']==null){
                        $return=$row;
                    }
                }else{
                    $return=$row;
                }
            }
        }
        mysqli_stmt_free_result($stmt);
        return $return;
    }
    
    function update( string $sql, bool $hasParams = False, int $nParams = 0, string $varTypes = '', array $params = array() )
    {
        $stmt = mysqli_prepare( $this->getConexion(), $sql );

        if($hasParams)
        {
            $this->bindParams( $varTypes, $nParams, $params, $stmt );
        }

        $update = mysqli_stmt_execute($stmt);
        mysqli_stmt_free_result($stmt);
        return $update;
    }
    
    function delete( string $sql, bool $hasParams = False, int $nParams = 0, string $varTypes = '', array $params = array() )
    {
        $stmt = mysqli_prepare( $this->getConexion(), $sql );

        if($hasParams)
        {
            $this->bindParams( $varTypes, $nParams, $params, $stmt );
        }

        $delete = mysqli_stmt_execute($stmt);
        mysqli_stmt_free_result($stmt);
        return $delete;
    }
    function autoIncrement( $id, $tabla )
    {
        $query = "SELECT MAX($id) MAX FROM $tabla";
        $stmt = mysqli_prepare( $this->getConexion(), $query );
       
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $contador);
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_free_result($stmt);

         return $contador + 1;
    }
    function getNumRows( string $tabla, bool $hasParams = False, int $nParams = 0, string $varTypes = '', array $params = array() )
    {
        $sql = "SELECT COUNT(CODIGO) CCONT FROM $tabla";
        $stmt = mysqli_prepare( $this->getConexion(), $sql );

        if($hasParams)
        {
            $this->bindParams( $varTypes, $nParams, $params, $stmt );
        }

        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $numrows);
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_free_result($stmt);

         return $numrows;

    }
    function beginTransaction()
    {
        mysqli_autocommit($this->getConexion(), FALSE);
        mysqli_begin_transaction($this->getConexion());
    }
    function Commit()
    {
        mysqli_commit($this->getConexion());
    }
    function rollBack()
    {
        mysqli_rollback($this->getConexion());
    }
    function getSeq(string $tabla, bool $hasParams = False, int $nParams = 0, string $varTypes = '', array $params = array())
    {
        $sql = "SELECT IFNULL(MAX(CONVERT(CODIGO, SIGNED INTEGER)), 0) MAX FROM $tabla";
        $stmt = mysqli_prepare( $this->getConexion(), $sql );

        if($hasParams)
        {
            $this->bindParams( $varTypes, $nParams, $params, $stmt );
        }

        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $contador);
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_free_result($stmt);

         return $contador + 1;
    }
    function real_escape_string($sql)
    {
        return mysqli_real_escape_string($this->getConexion(), $sql);
    }
}
