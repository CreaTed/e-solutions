-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-06-2019 a las 06:12:47
-- Versión del servidor: 10.1.29-MariaDB
-- Versión de PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `e-solutions`
--
CREATE DATABASE IF NOT EXISTS `e-solutions` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `e-solutions`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carro_compras`
--

CREATE TABLE `carro_compras` (
  `CODIGO` varchar(15) COLLATE utf8_bin NOT NULL,
  `CODUSU` varchar(15) COLLATE utf8_bin NOT NULL,
  `CODPROD` varchar(15) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `carro_compras`
--

INSERT INTO `carro_compras` (`CODIGO`, `CODUSU`, `CODPROD`) VALUES
('1', '1', '1'),
('2', '1', '2'),
('3', '1', '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `CODIGO` varchar(15) COLLATE utf8_bin NOT NULL,
  `NOMBRE` varchar(60) COLLATE utf8_bin NOT NULL,
  `PRECIO` float(9,2) NOT NULL,
  `DESCRIPCION` varchar(250) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`CODIGO`, `NOMBRE`, `PRECIO`, `DESCRIPCION`) VALUES
('1', 'COCO', 5000.00, 'El coco es una fruta tropical obtenida del cocotero Cocos nuciferas la palmera mas cultivada a nivel mundial.'),
('2', 'ACEITE DE COCO', 30000.00, 'El aceite de coco se ha convertido en un must have entre los amantes de la alimentaciÃ³n y la salud natural'),
('3', 'ACEITE DE COCO VIRGEN', 45000.00, 'Este aceite de coco sirve mucho para el tratamiento del cabello ya que posee minerales muy buenos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_imagen`
--

CREATE TABLE `producto_imagen` (
  `CODIGO` varchar(15) COLLATE utf8_bin NOT NULL,
  `URL` varchar(250) COLLATE utf8_bin NOT NULL,
  `CODPROD` varchar(15) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `producto_imagen`
--

INSERT INTO `producto_imagen` (`CODIGO`, `URL`, `CODPROD`) VALUES
('1', 'img/coco-fruta.png', '1'),
('2', 'img/coco2.jpg', '2'),
('3', 'img/tarro-coco.png', '3');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `carro_compras`
--
ALTER TABLE `carro_compras`
  ADD PRIMARY KEY (`CODIGO`),
  ADD KEY `CODPROD` (`CODPROD`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`CODIGO`);

--
-- Indices de la tabla `producto_imagen`
--
ALTER TABLE `producto_imagen`
  ADD PRIMARY KEY (`CODIGO`),
  ADD KEY `prod_img_ibfk_1` (`CODPROD`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `carro_compras`
--
ALTER TABLE `carro_compras`
  ADD CONSTRAINT `carro_compras_ibfk_1` FOREIGN KEY (`CODPROD`) REFERENCES `producto` (`CODIGO`);

--
-- Filtros para la tabla `producto_imagen`
--
ALTER TABLE `producto_imagen`
  ADD CONSTRAINT `prod_img_ibfk_1` FOREIGN KEY (`CODPROD`) REFERENCES `producto` (`CODIGO`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
