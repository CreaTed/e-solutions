<?php
    session_start();
    include_once '../lib/helpers.php';

    include_once '../view/Partials/head.php';
    include_once '../view/Partials/header.php';
    include_once '../view/Partials/login.php';

    if (!isset($_GET['modulo']) && !isset($_GET['controlador']) && !isset($_GET['funcion'])) 
    {
        $_GET['modulo'] = 'dashboard';
        $_GET['controlador'] = 'dashboard';
        $_GET['funcion'] = 'listar';
        $funcion = $_GET['funcion'];

        if( $_GET['funcion'] != "detalle" && $_GET['modulo'] != "admin" && $_GET['modulo'] != "contacto")
            include_once '../view/Partials/banner.php';

        include_once("../controller/" . ucwords($_GET['modulo']) . "/" . $_GET['controlador'] . "Controller.php");
        $nombreClase = $_GET['controlador'] . "Controller";
        $objControlador = new $nombreClase();

        if (method_exists($objControlador, $funcion))
        {
            $objControlador->$funcion();
        }   
    } 
    else 
    {
        $modulo = $_GET['modulo'];
        $controlador = $_GET['controlador'];
        $funcion = $_GET['funcion'];

        if (is_dir("../controller/" . ucwords($modulo))) 
        {

            if (file_exists("../controller/" . ucwords($modulo) . "/" . $controlador . "Controller.php")) 
            {
                if( $_GET['funcion'] != "detalle" && $_GET['modulo'] != "admin" && $_GET['modulo'] != "contacto")
                    include_once '../view/Partials/banner.php';
                    
                include_once("../controller/" . ucwords($modulo) . "/" . $controlador . "Controller.php");
                $nombreClase = $controlador . "Controller";
                $objControlador = new $nombreClase();

                if (method_exists($objControlador, $funcion))
                {
                    $objControlador->$funcion();
                } 
                else 
                {
                    die("La funcion especificada no existe");
                }
            }
            else 
            {
                die("El controlador especificado no existe.");
            }
        } 
        else 
        {
            die("El modulo especificado no existe.");
        }
    }


    include_once '../view/Partials/footer.php';

?>